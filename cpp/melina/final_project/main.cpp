#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

#define SIZE 19
#define COL_OFFSET 96
char table[SIZE][SIZE];

void print()
{
    cout << endl;
    //Print row labels
    for (int y=0; y<SIZE; y++)
    {
        int row = SIZE - (y+1);
        cout << row << "   ";
        if (row<10)
        {
            cout<<" ";
        }

        //Print spaces
        for (int x=0; x<SIZE; x++)
        {
            cout << table[y][x] << "   ";
        }
    }

    //Print column labels
    for (int x=0; x<SIZE; x++)
    {
        char col = COL_OFFSET+x+1;
        cout << col << "   ";
    }
    cout << endl << endl;
}

int wincheck ()
{
    int counter = 0;
    for (int r=0; r<SIZE; r++)            //Checking for a horizontal 5 in a row.
    {
        for (int c=0; c<SIZE; c++)
        {
            if (table[r][c] == 'X')
            {
                counter++;
            }
            else
            {
                counter=0;
            }
            if (counter==5)
            {
                return 100;
            }
        }
    }
    counter = 0;
    for (int c=0; c<SIZE; c++)           //Vertical Check
    {
        for (int r=0; r<SIZE; r++)
        {
            if (table[r][c] == 'X')
            {
                counter++;
            }
            else
            {
                counter=0;
            }
            if (counter==5)
            {
                return 100;
            }
        }
    }
    for (int r=4; r<35; r++)        //Positive diagonal check
    {
        counter = 0;
        for (int c=0; c<SIZE; c++)
        {
            if (table[r-c][c] == 'X')
            {
                counter++;
            }
            else
            {
                counter=0;
            }
            if (counter==5)
            {
                return 100;
            }
        }
    }
    for (int r=-20; r<35; r++)        //Negative diagonal check
    {
        counter = 0;
        for (int c=0; c<SIZE; c++)
        {
            if (table[r+c][c] == 'X')
            {
                counter++;
            }
            else
            {
                counter=0;
            }
            if (counter==5)
            {
                return 100;
            }
        }
    }

    return 50;
}

int singlespot(int x, int y)
{
    return SIZE*x + y;
}

int clevel1 ()
{
    srand(time(NULL));
    int x, y;

    do
    {
        x = rand() % SIZE;
        y = rand() % SIZE;
    } while (table[y][x]!='.');

    return singlespot(x, y);
}

int clevel3 ()
{
    for (int y=0; y<SIZE; y++)
    {
        for (int x=0; x<SIZE; x++)
        {
            if (table[y][x]!='.')
            {
                for (int dy=-1; dy<=1; dy++)
                {
                    for (int dx=-1; dx<=1; dx++)
                    {
                        if ((dx!=0 || dy!=0) && y+dy>=0 && y+dy<SIZE && x+dx>=0 && x+dx<SIZE && table[y+dy][x+dx]==table[y][x])
                        {
                            return singlespot(x + 2*dx, y + 2*dy);
                        }
                    }
                }
            }
        }
    }

    return clevel1();
}

int main ()
{
    srand(time(NULL));
    for (int y=0; y<SIZE;y++)
    {
        for (int x=0; x<SIZE; x++)
        {
            table[y][x]='.';
        }
    }

    int level;
    cout<<"Would you like to play level 1, 2, or 3?"<<endl;
    cin>>level;

    // char choice;
    // cout<<"Would you like to be O or X?"<<endl;
    // cin>>choice;

    print();

    cout<<"Your objective is to get 5 in a row (vertically, horizontally, or diagonally) before the computer does.\n";

    while (true)
    {
        char letter = 'z';
        int number = 0;
        cout<<"Where would you like to place an X?\n";

        //
        do
        {
            cout << "Column: please enter a lowercase letter between 'a' and '" << (char)(SIZE+COL_OFFSET) << "'.\n";
            cin >> letter;
        } while (letter<'a' || letter>(SIZE+COL_OFFSET));
        do
        {
            cout << "Row: please enter a number between 1 and " << SIZE << ".\n";
            cin >> number;
        } while (number < 1 || number > SIZE);

        //Converting player's coordinates into array coordinates
        int x = (int) letter - COL_OFFSET - 1;
        int y = SIZE - number;
        table[y][x] = 'X';

        //Computer move
        int singlespot;
        if (level==1)
        {
            singlespot = clevel1();
        }
        else if (level==3)
        {
            singlespot = clevel3();
        }
        x = singlespot / SIZE;
        y = singlespot % SIZE;
        table[y][x] = 'O';

        print();

        if (wincheck()==100)
        {
            cout<< "You win!!!" <<endl;
            return 0;
        }
    }

    return 0;
}
