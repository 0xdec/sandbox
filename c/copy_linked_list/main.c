#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LIST_LENGTH 10

typedef struct node_t node_t;

struct node_t {
  int data;
  node_t* next;
  node_t* arbit;
};

static node_t* src_list[LIST_LENGTH];
static node_t* dest_list[LIST_LENGTH];
static int dest_length = 0;


static void print_node(node_t* node) {
  if (node) {
    printf("%-10p = [%d]", node, node->data);
  } else {
    printf("(null)\t\t");
  }
}

static void print_list(node_t** list) {
  printf("node\t    data\tnode.next   data\tnode.arbit  data\n");
  for (int i = 0; i < LIST_LENGTH; i++) {
    print_node(list[i]);
    printf("\t");
    print_node(list[i]->next);
    printf("\t");
    print_node(list[i]->arbit);
    printf("\n");
  }
  printf("\n");
}

static void create_src(void) {
  srand(time(NULL));

  src_list[0] = malloc(sizeof(node_t));
  node_t* current = src_list[0];

  for (int i = 0; i < LIST_LENGTH; i++) {
    if (i > 0) {
      current->next = malloc(sizeof(node_t));
      current = current->next;
    }
    src_list[i] = current;
    current->data = i;
    current->next = NULL;
    current->arbit = NULL;
  }

  for (int i = 0; i < LIST_LENGTH; i++) {
    int arbit = rand() % (LIST_LENGTH + 1);
    src_list[i]->arbit = arbit < LIST_LENGTH ? src_list[arbit] : NULL;
  }

  printf("Initial List:\n");
  print_list(src_list);
}

static node_t* create_node(void) {
  dest_list[dest_length] = malloc(sizeof(node_t));
  return dest_list[dest_length++];
}

static int compare_lists(node_t* head) {
  printf("Copy Complete\n\n");

  printf("Source List:\n");
  print_list(src_list);

  printf("Destination List:\n");
  print_list(dest_list);

  for (int i = 0; i < LIST_LENGTH; i++) {
    int src_arbit = 0;
    int dest_arbit = LIST_LENGTH;

    if (!src_list[i]->arbit && !dest_list[i]->arbit) {
      src_arbit = dest_arbit;
    } else if (src_list[i]->arbit && dest_list[i]->arbit) {
      for (int j = 0; j < LIST_LENGTH; j++) {
        if (src_list[j] == src_list[i]->arbit) {
          src_arbit = j;
        }
        if (dest_list[j] == dest_list[i]->arbit) {
          dest_arbit = j;
        }
      }
    }

    if ((src_list[i]->data != dest_list[i]->data) ||
        (src_list[i]->next != (i < LIST_LENGTH-1 ? src_list[i+1] : NULL)) ||
        (dest_list[i]->next != (i < LIST_LENGTH-1 ? dest_list[i+1] : NULL)) ||
        (src_arbit != dest_arbit)) {
      printf("Copy Failed\n");
      return 1;
    }
  }

  printf("Copy Succeeded\n");
  return 0;
}

int main(void) {
  create_src();

  node_t* src = src_list[0];
  node_t* dest_head = create_node();
  node_t* dest = dest_head;

  // Copy src list to create dest list
  while (src) {
    // Copy data from src list
    dest->data = src->data;
    // Create the next node
    dest->next = src->next ? create_node() : NULL;
    // Point each dest.arbit at the corresponding src node
    dest->arbit = src;

    // Point each src.next at the corresponding dest node
    node_t* src_next = src->next;
    src->next = dest;
    dest = dest->next;
    src = src_next;
  }

  // Determine the proper dest.arbit pointers
  dest = dest_head;
  while (dest) {
    src = dest->arbit;
    // Point each dest.arbit at the corresponding src.arbit.next
    dest->arbit = src->arbit ? src->arbit->next : NULL;

    // Point each dest.next at the corresponding src.next
    node_t* dest_next = dest->next;
    dest->next = dest_next ? dest_next->arbit : NULL;
    dest = dest_next;
  }

  // Fix the src.next and dest.next pointers
  src = src_list[0];
  dest = dest_head;
  while (dest) {
    // Swap the src.next.next and dest.next pointers
    node_t* src_next = dest->next;
    src->next = src_next;
    dest->next = src_next ? src_next->next : NULL;
    src = src->next;
    dest = dest->next;
  }

  return compare_lists(dest_head);
}
