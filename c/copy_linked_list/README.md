# Copy Linked List

Make a copy of a singly linked list with arbitrary (arbit) pointers. Inspired by my friend's homework assignment.

## Build and Run
```sh
$ gcc main.c -o copy_linked_list
$ ./copy_linked_list
```
